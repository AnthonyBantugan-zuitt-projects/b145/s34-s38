const User = require("../models/User");
const bcrypt = require("bcrypt");
const router = require("../routes/userRoutes");
const auth = require("../auth");
const Course = require("../models/Course");

// Check if email exists\
module.exports.checkEmailExists = async (reqBody) => {
	return User.find({ email: reqBody.email }).then((result) => {
		if (result.length > 0) {
			return true;
		} else {
			return false;
		}
	});
};

// Registration
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobile: reqBody.mobile,
		password: bcrypt.hashSync(reqBody.password, 10),
	});
	return newUser.save().then((user, err) => {
		if (err) {
			return false;
		} else {
			return true;
		}
	});
};

// Login
module.exports.loginUser = async (reqBody) => {
	return User.findOne({ email: reqBody.email }).then((result) => {
		if (result == null) {
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(
				reqBody.password,
				result.password
			);
			if (isPasswordCorrect) {
				return { access: auth.createAccessToken(result) };
			} else {
				return false;
			}
		}
	});
};

// ACTIVITY for API DEVELOPMENT (part 2)===================================
//Retrieve details of user
// module.exports.getProfile = (userId) => {
// 	return User.findById(userId).then((result) => {
// 		result.password = "";
// 		return result;
// 	});
// };
// ===========================================

// Retrieve specific details
module.exports.getProfile = async (data) => {
	return User.findById(data.userId).then((result) => {
		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;
	});
};

// Enrollment
module.exports.enroll = async (data) => {
	if (data.payload.isAdmin === true) {
		return false;
	} else {
		let isUserUpdated = await User.findById(data.userId).then((user) => {
			user.enrollments.push({ courseId: data.courseId });
			return user.save().then((user, err) => {
				if (err) {
					return false;
				} else {
					return true;
				}
			});
		});

		let isCourseUpdated = await Course.findById(data.courseId).then(
			(course) => {
				course.enrollees.push({ userId: data.userId });
				return course.save().then((course, err) => {
					if (err) {
						return false;
					} else {
						return true;
					}
				});
			}
		);

		if (isUserUpdated && isCourseUpdated) {
			return true;
		} else {
			return false;
		}
	}
};
