const Course = require("../models/Course");
const router = require("../routes/courseRoutes");
const auth = require("../auth");

// ACTIVITY for API Development (Part 3)=================
// Add a course
// module.exports.addCourse = (reqBody) => {
// 	let newCourse = new Course({
// 		name: reqBody.name,
// 		description: reqBody.description,
// 		price: reqBody.price,
// 	});
// 	return newCourse.save().then((course, err) => {
// 		if (err) {
// 			return false;
// 		} else {
// 			return course;
// 		}
// 	});
// };

module.exports.addCourse = async (reqBody, userData) => {
	if (userData.isAdmin === true) {
		let newCourse = new Course({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price,
		});
		return newCourse.save().then((course, err) => {
			if (err) {
				return false;
			} else {
				return course;
			}
		});
	} else {
		return `${userData.email} is not authorized`;
	}
};
// ===================================================

// Retrieve all courses
module.exports.getAllCourses = async (user) => {
	if (user.isAdmin === true) {
		return Course.find({}).then((result) => {
			return result;
		});
	} else {
		return `${user.email} is not authorized`;
	}
};

// Retrieval of active courses
module.exports.getAllActive = () => {
	return Course.find({ isActive: true }).then((result) => {
		return result;
	});
};

// Retrieve specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then((result) => {
		return result;
	});
};

// Update Course
module.exports.updateCourse = (data) => {
	return Course.findById(data.courseId).then((result, err) => {
		if (data.payload.isAdmin === true) {
			result.name = data.updatedCourse.name;
			result.description = data.updatedCourse.description;
			result.price = data.updatedCourse.price;
			return result.save().then((updatedCourse, err) => {
				if (err) {
					return false;
				} else {
					return updatedCourse;
				}
			});
		} else {
			return false;
		}
	});
};

// ACTIVITY for API Development (Part 4)=================
// Archive Course
module.exports.archiveCourse = (data) => {
	return Course.findById(data.courseId).then((result, err) => {
		if (data.payload.isAdmin === true) {
			result.isActive = data.reqBody.isActive;
			return result.save().then((archivedCourse, err) => {
				if (err) {
					return err;
				} else {
					return archivedCourse;
				}
			});
		}
	});
};
