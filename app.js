// [SECTION] Dependencies and Modules
const express = require("express");

const mongoose = require("mongoose");
const cors = require("cors");
const dotenv = require("dotenv");
const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");

// [SECTION] Environment Variables Setup
// configure the application in order for it to recognize and identify the necessary components needed to build the app successfully
dotenv.config();

// [SECTION] Server Setup
const app = express();
app.use(
	cors({
		origin: "*",
	})
);
const port = process.env.PORT;

// [SECTION] Database Connect
mongoose
	.connect(process.env.MONGO_URL, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
	})
	.then(() => {
		console.log(`Connected to MongoDB`);
	})
	.catch((err) => {
		console.log(err);
	});

// [SECTION] Middelwares
app.use(express.json());
app.use(
	express.urlencoded({
		extended: true,
	})
);

// [SECTION] Server Routes

app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

// [SECTION] Server Response
app.listen(port, () => {
	console.log(`Server running at port ${port}`);
});
