const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers");
const auth = require("../auth");

// ACTIVITY for API Development (Part 3)=================
// Add a course
// router.post("/", (req, res) => {
// 	courseController
// 		.addCourse(req.body)
// 		.then((resultFromController) => res.send(resultFromController));
// });

router.post("/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	courseController
		.addCourse(req.body, userData)
		.then((resultFromController) => res.send(resultFromController));
});
// ===================================================

// Retrieve all courses
router.get("/all", auth.verify, (req, res) => {
	const data = auth.decode(req.headers.authorization);
	courseController
		.getAllCourses(data)
		.then((resultFromController) => res.send(resultFromController));
});

// Retrieval of active courses
router.get("/", (req, res) => {
	courseController
		.getAllActive()
		.then((resultFromController) => res.send(resultFromController));
});

// Retrieve specific course
router.get("/:courseId", (req, res) => {
	courseController
		.getCourse(req.params)
		.then((resultFromController) => res.send(resultFromController));
});

// Update course
router.put("/:courseId", auth.verify, (req, res) => {
	const data = {
		courseId: req.params.courseId,
		payload: auth.decode(req.headers.authorization),
		updatedCourse: req.body,
	};
	courseController
		.updateCourse(data)
		.then((resultFromController) => res.send(resultFromController));
});
module.exports = router;

// ACTIVITY for API Development (Part 4)=================
// Archiving Course
router.put("/:courseId/archive", auth.verify, (req, res) => {
	const data = {
		courseId: req.params.courseId,
		payload: auth.decode(req.headers.authorization),
		reqBody: req.body,
	};
	courseController
		.archiveCourse(data)
		.then((resultFromController) => res.send(resultFromController));
});
